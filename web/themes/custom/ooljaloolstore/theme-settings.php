<?php
/**
 * Implementation of hook_form_system_theme_settings_alter()
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 *
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */


 function ooljalool_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
    // Theme info
  $theme = \Drupal::theme()->getActiveTheme()->getName();
  
  $form['fonts_and_icons']['font_set'] = array(
    '#type' => 'select',
    '#title' => t('Font libraries'),
    '#default_value' => theme_get_setting('font_set'),
    '#empty_option' => t('None'),
    '#description' => t('A few predefined font libraries delivered from Google.<br/>All fonts are loaded with Regular, Italic and Bold variants.'),
    '#options' => array(
      'ibm_plex_sans' => t('IBM Plex Sans'),
      'lato' => t('Lato'),
      'montserrat' => t('Montserrat'),
      'open_sans' => t('Open Sans'),
      'raleway' => t('Raleway'),
      'roboto' => t('Roboto'),
      'quicksand' => t('Quick Sand'),
      'pintails' => t('Pintails'),
      'doto' => t('Doto'),
    ),
  );

 }