<?php

namespace Drupal\ooljalool_core\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_store\Entity\StoreInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Controller for handling AJAX add to cart functionality.
 */
class ShopProductController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * Constructs a new ShopProductController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The cart manager.
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart provider.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    CartManagerInterface $cart_manager,
    CartProviderInterface $cart_provider
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->cartManager = $cart_manager;
    $this->cartProvider = $cart_provider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('commerce_cart.cart_manager'),
      $container->get('commerce_cart.cart_provider')
    );
  }

  /**
   * Adds a product variation to the cart via AJAX.
   *
   * @param string $pid
   *   The product variation ID.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AJAX response.
   */
  public function addToCart($pid) {
    $response = new AjaxResponse();

    try {
      if (empty($pid)) {
        throw new \InvalidArgumentException('Product variation ID is required.');
      }

      // Load the default store.
      $store = $this->entityTypeManager
        ->getStorage('commerce_store')
        ->loadDefault();

      if (!$store instanceof StoreInterface) {
        throw new \RuntimeException('No default store found.');
      }

      // Load the product variation.
      $product_variation = $this->entityTypeManager
        ->getStorage('commerce_product_variation')
        ->load($pid);

      if (!$product_variation) {
        throw new \RuntimeException('Product variation not found.');
      }

      // Get or create the cart.
      $cart = $this->cartProvider->getCart('default', $store);
      if (!$cart) {
        $cart = $this->cartProvider->createCart('default', $store);
      }

      // Create and add the order item.
      $order_item = $this->entityTypeManager
        ->getStorage('commerce_order_item')
        ->create([
          'type' => 'default',
          'purchased_entity' => $product_variation->id(),
          'quantity' => 1,
          'unit_price' => $product_variation->getPrice(),
        ]);

      $order_item->save();
      $this->cartManager->addOrderItem($cart, $order_item);

      // Get updated cart block content
      $block_manager = \Drupal::service('plugin.manager.block');
      $config = [];
      $block_plugin = $block_manager->createInstance('commerce_cart', $config);
      $build = $block_plugin->build();
      $rendered_block = \Drupal::service('renderer')->render($build);

      // Add commands to update the cart block and show message
      $response->addCommand(
        new HtmlCommand('.cart--cart-block', $rendered_block)
      );
      
      // Update the add to cart button
      $response->addCommand(
        new HtmlCommand('.-add-to-cart', '<span class="-added">Added to cart</span>')
      );
      
      // Show success message
      $response->addCommand(
        new MessageCommand($this->t('Product added to your cart.'), NULL, ['type' => 'status'])
      );
    }
    catch (\Exception $e) {
      $response->addCommand(
        new MessageCommand($this->t('Unable to add the product to your cart. Please try again.'), NULL, ['type' => 'error'])
      );
      \Drupal::logger('ooljalool_core')->error($e->getMessage());
    }

    return $response;
  }

}